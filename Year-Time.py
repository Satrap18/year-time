from tkinter import * 
from tkinter import ttk
from time import strftime


window = Tk()
window.title('The Watch')
window.resizable(False,False)
window.config(bg='purple')

def my_time():
    time = strftime('%H:%M:%S %p')
    Year = strftime('%Y %B%d %A')
    lbl.config(text=Year)
    lbl.after(1000,my_time)
    lbl1.config(text=time)
    lbl1.after(1000,my_time)
	

lbl = Label(window,font=('times',52,'bold'),bg='purple',fg='white')
lbl.pack()
lbl1 = Label(window,font=('times',52,'bold'),bg='purple',fg='white')
lbl1.pack()

my_time()
window.mainloop()
